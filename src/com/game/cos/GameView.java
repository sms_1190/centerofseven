package com.game.cos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.game.exception.NullCanvasException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

@SuppressLint("DrawAllocation")
public class GameView extends SurfaceView {

	private SurfaceHolder holder;
	private GameLoopThread gameLoopThread;
	private String[] cards = new String[52];

	private Player player1=new Player();
	private Player player2=new Player();
	private Player player3=new Player();
	private Player player4=new Player();
	private List<String> p1= new ArrayList<String>();
	private List<String> p2= new ArrayList<String>();
	private List<String> p3= new ArrayList<String>();
	private List<String> p4= new ArrayList<String>();

	private List<String> r1 = new ArrayList<String>();
	private List<String> r2 = new ArrayList<String>();
	private List<String> r3 = new ArrayList<String>();

	private int turn = 0;
	private int[] pscore = { 0, 0, 0, 0 };
	private Canvas c;
	Bitmap tempbitmap = null;
	private Integer passButtonPositionX = 0;
	private Integer passButtonPositionY = 0;

	private float startingX = 0;
	private float endingX = 0;
	private float startingY = 0;
	private float endingY = 0;
	private boolean flag = true;

	private float card_size_x = 0;
	private float card_size_y = 0;

	private float gapBetweenTwoCards = 40;
	private float gapBetweenTwoCardsForOthers = 20;
	private float player2position = 0;
	private float player4position = 0;

	private int p1x;
	private int p2y;
	private int p3x;
	private int p4y;

	private String message = "";

	private DatabaseHandler db;
	private Context context;

	public GameView(Context context, DatabaseHandler db) {
		super(context);
		this.db = db;
		this.context=context;
		gameLoopThread = new GameLoopThread(this);
		holder = getHolder();
		holder.addCallback(new SurfaceHolder.Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				boolean retry = true;
				gameLoopThread.setRunning(false);
				while (retry) {
					try {
						gameLoopThread.join();
						retry = false;
					} catch (InterruptedException e) {
					}
				}
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				suffleCards();
				gameLoopThread.setRunning(true);
				gameLoopThread.start();
				checkSevenOfHeart();
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
			}
		});
	}

	public void suffleCards() {

		int r = 0;
		for (Integer i = 1; i < 14; i++) {
			cards[r] = "h" + i.toString();
			r = r + 1;
		}
		for (Integer i = 1; i < 14; i++) {
			cards[r] = "d" + i.toString();
			r = r + 1;
		}
		for (Integer i = 1; i < 14; i++) {
			cards[r] = "c" + i.toString();
			r = r + 1;
		}
		for (Integer i = 1; i < 14; i++) {
			cards[r] = "s" + i.toString();
			r = r + 1;
		}
		for (int i = 0; i < 4; i++) {
			r1.add("blank");
			r2.add("blank");
			r3.add("blank");
		}
		Random rgen = new Random(); // Random number generator
		// --- Shuffle by exchanging each element randomly
		for (int i = 0; i < cards.length; i++) {
			int randomPosition = rgen.nextInt(cards.length);
			String temp = cards[i];
			cards[i] = cards[randomPosition];
			cards[randomPosition] = temp;
		}
		for (int i = 0; i < 52; i++) {
			if (i < 13) {
				p1.add(cards[i]);
			} else if (i >= 13 && i < 26) {
				p2.add(cards[i]);
			} else if (i >= 26 && i < 39) {
				p3.add(cards[i]);
			} else if (i >= 39 && i < 52) {
				p4.add(cards[i]);
			}
		}
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		this.c = canvas;
		try {
			if (this.c == null) {
				throw new NullCanvasException();
			}

			Paint bg = new Paint();
			Rect BG = new Rect();
			bg.setColor(Color.rgb(0, 153, 0));
			bg.setStyle(Paint.Style.FILL_AND_STROKE);
			BG = new Rect();
			BG.set(0, 0, canvas.getWidth(), canvas.getHeight());
			canvas.drawRect(BG, bg);
			Paint textpaint = new Paint();
			textpaint.setAntiAlias(true);
			textpaint.setTextSize(20);
			textpaint.setColor(Color.BLACK);
			/*int rid1 = getResources().getIdentifier("home_bg", "drawable",
					"com.game.cos");
			BitmapFactory.Options myOptions1 = new BitmapFactory.Options();
			
			
			Bitmap bitmapbg=BitmapFactory.decodeResource(getResources(),
					rid1, null);
			
			System.out.println(bitmapbg.getWidth());
			System.out.println(bitmapbg.getHeight());
			 
			 canvas.drawBitmap(bitmapbg,-30,-30, null);
*/
			if (this.flag == false) {
				canvas.drawText("This Game Score :", 200, 100, textpaint);
				canvas.drawText("Player 1 : " + pscore[0], 200, 150, textpaint);
				canvas.drawText("Player 2 : " + pscore[1], 200, 200, textpaint);
				canvas.drawText("Player 3 : " + pscore[2], 200, 250, textpaint);
				canvas.drawText("Player 4 : " + pscore[3], 200, 300, textpaint);
				
			/*	if(db!=null){
					Score score = new Score();
				
				score.setId(1);
				score.setScore(pscore[0]);
				db.deleteScore(score);
				db.addScore(score);
				score.setId(2);
				score.setScore(pscore[1]);
				db.deleteScore(score);
				db.addScore(score);
				score.setId(3);
				score.setScore(pscore[2]);
				db.deleteScore(score);
				db.addScore(score);
				score.setId(4);
				score.setScore(pscore[3]);
				db.deleteScore(score);
				db.addScore(score);
				int count = db.getScoresCount();
				if (count == 0) {
					
					score = new Score();
					score.setId(1);
					score.setScore(pscore[0]);
					db.deleteScore(score);
					db.addScore(score);
					score.setId(2);
					score.setScore(pscore[1]);
					db.deleteScore(score);
					db.addScore(score);
					score.setId(3);
					score.setScore(pscore[2]);
					db.deleteScore(score);
					db.addScore(score);
					score.setId(4);
					score.setScore(pscore[3]);
					db.deleteScore(score);
					db.addScore(score);
				} else {
					Score score1=db.getscore(1);
					score = new Score();
					score.setId(1);
					score.setScore((score1.getScore()+pscore[0]));
					db.updateScore(score);
					score1=db.getscore(2);
					score.setId(2);
					score.setScore((score1.getScore()+pscore[1]));
					db.updateScore(score);
					score1=db.getscore(3);
					score.setId(3);
					score.setScore((score1.getScore()+pscore[2]));
					db.updateScore(score);
					score1=db.getscore(4);
					score.setId(4);
					score.setScore((score1.getScore()+pscore[3]));
					db.updateScore(score);
				}
				canvas.drawText("Total Score :", 200, 350, textpaint);
				canvas.drawText("Player 1 : " + db.getscore(1).getScore(), 200, 400, textpaint);
				canvas.drawText("Player 2 : " + db.getscore(2).getScore(), 200, 450, textpaint);
				canvas.drawText("Player 3 : " + db.getscore(3).getScore(), 200, 500, textpaint);
				canvas.drawText("Player 4 : " + db.getscore(4).getScore(), 200, 550, textpaint);
				db=null;
				}*/
			} else {
				int rectx;
				int recty;
				int rid = getResources().getIdentifier("card_bg", "drawable",
						"com.game.cos");
				BitmapFactory.Options myOptions = new BitmapFactory.Options();
				myOptions.inDither = true;
				myOptions.inScaled = false;
				myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
				myOptions.inPurgeable = true;
				Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
						rid, myOptions);
				tempbitmap = bitmap;
				String s1;
				card_size_y = (canvas.getHeight()
						- (5 * tempbitmap.getHeight()) - 30) / 5;
				card_size_x = (canvas.getWidth() - (12 * gapBetweenTwoCards) - tempbitmap
						.getWidth()) / 13;

				p1x = (int) ((canvas.getWidth()
						- (gapBetweenTwoCards * (p1.size() - 1))
						- tempbitmap.getWidth() - card_size_x) / 2);
				startingX = p1x;
				player2position = (int) ((canvas.getWidth()
						- (gapBetweenTwoCards * 12) - tempbitmap.getWidth() - card_size_x) / 2);
				player4position = player2position + (gapBetweenTwoCards * 12)
						+ tempbitmap.getWidth() + card_size_x;
				updateEndingX();
				canvas.drawText(message, 10, 30, textpaint);
				SortList sortList = new SortList();
				p1 = sortList.sort(p1);

				for (Integer i = 0; i < p1.size(); i++) {
					s1 = p1.get(i).toString();
					bitmap=getBitmap(s1);
					bitmap = Bitmap.createScaledBitmap(bitmap,
							(int) (bitmap.getWidth() + card_size_x),
							(int) (bitmap.getHeight() + card_size_y), true);
					canvas.drawBitmap(bitmap, p1x,
							canvas.getHeight() - bitmap.getHeight()
									- card_size_y, null);
					p1x = (int) (p1x + gapBetweenTwoCards);
				}

				passButtonPositionX = (int) (player4position + 10);
				passButtonPositionY = canvas.getHeight() - 70;

				// player 2
				p2y = (int) ((canvas.getHeight()
						- (gapBetweenTwoCardsForOthers * (p2.size() - 1))
						- tempbitmap.getHeight() - card_size_y) / 2);
				Collections.sort(p2);
				for (Integer i = 0; i < p2.size(); i++) {
					s1 = "card_bg";
					bitmap=getBitmap(s1);
					bitmap = Bitmap.createScaledBitmap(bitmap,
							(int) (bitmap.getWidth() + card_size_x),
							(int) (bitmap.getHeight() + card_size_y), true);
					Matrix matrix = new Matrix();
					matrix.postRotate(90);
					bitmap = Bitmap
							.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
									bitmap.getHeight(), matrix, true);
					canvas.drawBitmap(bitmap,
							player2position - tempbitmap.getHeight()
									- card_size_y - 10, p2y, null);
					p2y = (int) (p2y + gapBetweenTwoCardsForOthers);
				}

				// player 3
				p3x = (int) ((canvas.getWidth()
						- (gapBetweenTwoCardsForOthers * (p3.size() - 1))
						- tempbitmap.getWidth() - card_size_x) / 2);

				Collections.sort(p3);
				for (Integer i = 0; i < p3.size(); i++) {
					s1 = "card_bg";
					bitmap=getBitmap(s1);
					bitmap = Bitmap.createScaledBitmap(bitmap,
							(int) (bitmap.getWidth() + card_size_x),
							(int) (bitmap.getHeight() + card_size_y), true);
					canvas.drawBitmap(bitmap, p3x, 5, null);
					p3x = (int) (p3x + gapBetweenTwoCardsForOthers);
				}

				// displaying middle rectangles
				rectx = (int) ((canvas.getWidth() - (((tempbitmap.getWidth() + card_size_x) * 4) + (3 * 10))) / 2);
				recty = (int) ((canvas.getHeight() - (((tempbitmap.getHeight() + card_size_y) * 3) + (2 * 5))) / 2);
				for (int e = 0; e < 4; e++) {
					s1 = r1.get(e).toString();
					bitmap=getBitmap(s1);
					bitmap = Bitmap.createScaledBitmap(bitmap,

					(int) (bitmap.getWidth() + card_size_x),
							(int) (bitmap.getHeight() + card_size_y), true);
					canvas.drawBitmap(bitmap, rectx, recty, null);
					rectx = (int) (rectx + tempbitmap.getWidth() + card_size_x + 10);
				}
				rectx = (int) ((canvas.getWidth() - (((tempbitmap.getWidth() + card_size_x) * 4) + (3 * 10))) / 2);
				recty = (int) (recty + tempbitmap.getHeight() + card_size_y);
				for (int e = 0; e < 4; e++) {
					s1 = r2.get(e).toString();
					bitmap=getBitmap(s1);
					bitmap = Bitmap.createScaledBitmap(bitmap,

					(int) (bitmap.getWidth() + card_size_x),
							(int) (bitmap.getHeight() + card_size_y), true);
					canvas.drawBitmap(bitmap, rectx, recty, null);
					rectx = (int) (rectx + tempbitmap.getWidth() + card_size_x + 10);
				}
				rectx = (int) ((canvas.getWidth() - (((tempbitmap.getWidth() + card_size_x) * 4) + (3 * 10))) / 2);
				recty = (int) (recty + tempbitmap.getHeight() + card_size_y);
				for (int e = 0; e < 4; e++) {
					s1 = r3.get(e).toString();
					bitmap=getBitmap(s1);
					bitmap = Bitmap.createScaledBitmap(bitmap,

					(int) (bitmap.getWidth() + card_size_x),
							(int) (bitmap.getHeight() + card_size_y), true);
					canvas.drawBitmap(bitmap, rectx, recty, null);
					rectx = (int) (rectx + tempbitmap.getWidth() + card_size_x + 10);
				}

				// player 4
				p4y = (int) ((canvas.getHeight()
						- (gapBetweenTwoCardsForOthers * (p4.size() - 1))
						- tempbitmap.getHeight() - card_size_y) / 2);
				Collections.sort(p4);
				for (Integer i = 0; i < p4.size(); i++) {
					s1 = "card_bg";
					bitmap=getBitmap(s1);
					bitmap = Bitmap.createScaledBitmap(bitmap,
							(int) (bitmap.getWidth() + card_size_x),
							(int) (bitmap.getHeight() + card_size_y), true);
					Matrix matrix = new Matrix();
					matrix.preRotate(90);
					bitmap = Bitmap
							.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
									bitmap.getHeight(), matrix, true);
					canvas.drawBitmap(bitmap, player4position + 10, p4y, null);
					p4y = (int) (p4y + gapBetweenTwoCardsForOthers);
				}
				s1 = "pass";
				bitmap=getBitmap(s1);
				bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(),
						bitmap.getHeight(), true);
				canvas.drawBitmap(bitmap, passButtonPositionX,
						passButtonPositionY, null);
			}
		} catch (NullCanvasException e) {
			System.out.println(e);
		}
	}

	private Bitmap getBitmap(String s1) {
		int rid = getResources().getIdentifier(s1, "drawable",
				"com.game.cos");
		
		Options myOptions = new BitmapFactory.Options();
		myOptions.inDither = true;
		myOptions.inScaled = false;
		myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
		myOptions.inPurgeable = true;
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), rid,
				myOptions);
		return bitmap;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			float Xcordi;
			float Ycordi;
			Xcordi = event.getX();
			Ycordi = event.getY();

			if(this.turn==2 || this.turn==3 || this.turn==4){
				
			}
			else if (this.turn == 1) {

				if (checkPassButton(Xcordi, Ycordi)) {
					if (checkchoices() == 0) {
						this.turn = 2;
						continueGame();
					}
				}

				updateEndingX();

				if (Xcordi > startingX && Xcordi < endingX
						&& Ycordi > startingY && Ycordi < endingY) {
					String card;
					card = checkClickedCard(Xcordi, Ycordi);
					if (card.equals("pass")) {
						this.message = "Choose other";

					} else {

						update_states(p1.indexOf(card), card, 1);
						this.turn = 2;
						continueGame();
					}
				}
			}
			else{
				
			}
		}
		return super.onTouchEvent(event);

	}

	private int checkchoices() {
		MiniMax miniMax = new MiniMax();
		if (miniMax.findlegalMoves(p1, r1, r2, r3) == 1)
			return 1;
		else
			return 0;
	}

	public String checkClickedCard(float x, float y) {
		String card = "pass";
		float tempx = x - startingX;
		int cardposition = (int) Math.floor(tempx / gapBetweenTwoCards);
		if (cardposition >= p1.size())
			cardposition = p1.size() - 1;

		card = p1.get(cardposition);
		if (checkLegalMove(card)) {

			return card;
		} else {
			return "pass";
		}
	}

	public boolean checkLegalMove(String card) {
		String cardface = card.substring(0, 1);

		int cardnumber = Integer.parseInt(card.substring(1));
		int position = 0;
		if (cardface.equals("h")) {
			position = 0;
		} else if (cardface.equals("s")) {
			position = 1;
		} else if (cardface.equals("d")) {
			position = 2;
		} else if (cardface.equals("c")) {
			position = 3;
		}

		int[] tempr = { 0, 0, 0 };

		if (r2.get(position).equals("blank")) {
			if (cardnumber == 7) {
				return true;
			}
		} else {

			// check for upper part
			if (cardnumber > 7 && !r1.get(position).equals("blank")) {
				tempr[0] = Integer.parseInt(r1.get(position).substring(1));
				if ((cardnumber) == (tempr[0] + 1)) {
					return true;
				}
			} else if (cardnumber > 7 && r1.get(position).equals("blank")) {
				if ((cardnumber) == 8) {
					return true;
				}
			}
			// check for lower part
			if (cardnumber < 7 && !r3.get(position).equals("blank")) {
				tempr[2] = Integer.parseInt(r3.get(position).substring(1));
				if ((cardnumber) == (tempr[2] - 1)) {
					return true;
				}
			} else if (cardnumber < 7 && r3.get(position).equals("blank")) {
				if ((cardnumber) == 6) {
					return true;
				}
			}
		}

		return false;
	}

	// fourth player's method
	private void FourthPlayerCall() {
		String card;
		card = callingminimax(p4);
		if (card == "pass") {
		} else {
			int location = p4.indexOf(card);
			update_states(location, card, 4);
		}
		this.turn = 1;
		continueGame();
	}

	// third player's method
	private void ThirdPlayerCall() {
		String card;
		card = callingminimax(p3);
		if (card == "pass") {
		} else {
			int location = p3.indexOf(card);
			update_states(location, card, 3);
		}
		this.turn = 4;
		continueGame();
	}

	// second player's method
	private void SecondPlayerCall() {
		String card;
		card = callingminimax(p2);
		if (card == "pass") {
		} else {
			int location = p2.indexOf(card);
			update_states(location, card, 2);
		}
		this.turn = 3;
		continueGame();
	}

	// min max algorithm for players' move
	private String callingminimax(List<String> p) {
		MiniMax miniMax = new MiniMax();
		String card = miniMax.findbestMove(p, r1, r2, r3);
		return card;
	}

	// Method for checking Pass Button of player 1
	private boolean checkPassButton(float xcordi, float ycordi) {
		float Xcordi1;
		float Ycordi1;
		Xcordi1 = passButtonPositionX + tempbitmap.getWidth();
		Ycordi1 = passButtonPositionY + tempbitmap.getHeight();
		if ((xcordi >= passButtonPositionX) && (ycordi >= passButtonPositionY)
				&& (xcordi <= Xcordi1) && (ycordi <= Ycordi1)) {
			return true;
		} else {
			return false;
		}
	}

	public void check_for_game_over() {
		if (p1.isEmpty() || p2.isEmpty() || p3.isEmpty() || p4.isEmpty()) {
			calculate_players_score();
		}
	}

	public void calculate_players_score() {
		CalculateScore calculateScore = new CalculateScore();
		pscore[0] = calculateScore.calcScore(p1);
		pscore[1] = calculateScore.calcScore(p2);
		pscore[2] = calculateScore.calcScore(p3);
		pscore[3] = calculateScore.calcScore(p4);
		this.flag = false;

		// update_players_score(pscore);
	}

	public void update_players_score(int[] pscore) {

	}

	public void update_faces_states(String card) {
		int position = 0;
		if (card.startsWith("h")) {
			position = 0;
		} else if (card.startsWith("s")) {
			position = 1;
		} else if (card.startsWith("c")) {
			position = 3;
		} else if (card.startsWith("d")) {
			position = 2;
		}
		if (Integer.parseInt(card.substring(1)) > 7) {
			r1.set(position, card);
		} else if (Integer.parseInt(card.substring(1)) == 7) {
			r2.set(position, card);
		} else if (Integer.parseInt(card.substring(1)) < 7) {
			r3.set(position, card);
		}
	}

	public void update_players_states(int location, int player) {
		if (player == 1)
			p1.remove(location);
		else if (player == 2)
			p2.remove(location);
		else if (player == 3)
			p3.remove(location);
		else if (player == 4)
			p4.remove(location);
	}

	public void update_states(int location, String card, int player) {
		update_faces_states(card);
		update_players_states(location, player);

	}
	public void updateEndingX() {
		endingX = startingX + (gapBetweenTwoCards * (p1.size() - 1))
				+ tempbitmap.getWidth() + card_size_x;
		startingY = this.c.getHeight() - tempbitmap.getHeight() - card_size_y;
		endingY = startingY + tempbitmap.getHeight() + card_size_y;
	}

	// method for checking seven of Heart
	private void checkSevenOfHeart() {
		for (int i = 0; i < p1.size(); i++) {
			if (p1.get(i).equals("h7")) {
				start_game(i, 1);
				break;
			}
			if (p2.get(i).equals("h7")) {
				start_game(i, 2);
				break;
			}
			if (p3.get(i).equals("h7")) {
				start_game(i, 3);
				break;
			}
			if (p4.get(i).equals("h7")) {
				start_game(i, 4);
				break;
			}
		}
	}

	// method for starting game after checking seven of heart
	private void start_game(int location, int player) {
		update_states(location, "h7", player);
		if (player == 1)
			this.turn = 2;
		else if (player == 2)
			this.turn = 3;
		else if (player == 3)
			this.turn = 4;
		else if (player == 4)
			this.turn = 1;

		if (this.turn == 1) {
			// waiting for user
		}
		if (this.turn == 2) {
			// call player 2's method
			SecondPlayerCall();
		}
		if (this.turn == 3) {
			// call player 3's method
			ThirdPlayerCall();
		}
		if (this.turn == 4) {
			// call player 4's method
			FourthPlayerCall();
		}
	}
	private void continueGame() {
		
		if (this.flag == false) {
		} else {
			try {
				this.gameLoopThread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.message = "";
			check_for_game_over();
			if (turn == 1) {
				// waiting for user
				this.message = "Your turn";
			}
			if (turn == 2) {
				// call player 2's method
				this.message = "Second Player";
				SecondPlayerCall();
			}
			if (turn == 3) {
				// call player 3's method
				this.message = "Third Player";
				ThirdPlayerCall();
			}
			if (turn == 4) {
				// call player 4's method
				this.message = "Fourth Player";
				FourthPlayerCall();
			}
		}
		
	}
	// add menu
}
