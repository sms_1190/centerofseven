package com.game.cos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import com.game.cos.R;

public class Game extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setFullScreen();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		 DatabaseHandler db = new DatabaseHandler(this);
		 
		setContentView(new GameView(this,db));
	}
	
	/**
	 * Set the full screen mode for the activity
	 */
	private void setFullScreen() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	
}
