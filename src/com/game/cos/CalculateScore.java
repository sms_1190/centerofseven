package com.game.cos;

import com.game.cos.R;
import java.util.List;

public class CalculateScore {


	public int calcScore(List<String> p) {
		int val = 0;
		int score = 0;
		if(!p.isEmpty()){
		for (int i = 0; i < p.size(); i++) {
			val = Integer.parseInt(p.get(i).substring(1));
			if (val == 1) {
				score += 15;
			} else if (val >= 10) {
				score += 10;
			} else {
				score += val;
			}
		}
		}
		return score;
	}
}
