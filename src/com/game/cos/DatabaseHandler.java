package com.game.cos;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "scoresManager";

	// Contacts table name
	private static final String TABLE_SCORES = "scores";

	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_SCORES + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " INTEGER"+ ")";
		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORES);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new contact
	void addScore(Score score) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, score.getScore()); // Contact Name
		values.put(KEY_ID,score.getId());
		// Inserting Row
		db.insert(TABLE_SCORES, null, values);
		db.close(); // Closing database connection
	}

	// Getting single contact
	Score getscore(Integer id) {
		SQLiteDatabase db = this.getReadableDatabase();
System.out.println(id);
		Cursor cursor = db.query(TABLE_SCORES, new String[] { KEY_ID,
				KEY_NAME }, KEY_ID + "=?", new String[] { id.toString() },
				null, null, null, null);
		System.out.println(cursor.getCount());
		if (cursor != null)
			cursor.moveToFirst();
		
		Score score = new Score(Integer.parseInt(cursor.getString(1)),
				Integer.parseInt(cursor.getString(0)));
		// return contact
		return score;
	}

	// Getting All Contacts
	public List<Score> getAllScores() {
		List<Score> scoreList = new ArrayList<Score>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SCORES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Score score = new Score();
				score.setId(Integer.parseInt(cursor.getString(0)));
				score.setScore(Integer.parseInt(cursor.getString(1)));
				
				// Adding contact to list
				scoreList.add(score);
			} while (cursor.moveToNext());
		}

		// return contact list
		return scoreList;
	}

	// Updating single contact
	public int updateScore(Score score) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, score.getScore());
		
		// updating row
		return db.update(TABLE_SCORES, values, KEY_ID + " = ?",
				new String[] { String.valueOf(score.getId()) });
	}

	// Deleting single contact
	public void deleteScore(Score score) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_SCORES, KEY_ID + " = ?",
				new String[] { String.valueOf(score.getId()) });
		db.close();
	}

	// Getting contacts Count
	public int getScoresCount() {
		String countQuery = "SELECT  * FROM " + TABLE_SCORES;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		

		// return count
		return cursor.getCount();
	}

}