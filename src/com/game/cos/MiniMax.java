package com.game.cos;

import com.game.cos.R;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MiniMax {

	public String findbestMove(List<String> p, List<String> r1,
			List<String> r2, List<String> r3) {
		String card = "pass";
		List<String> legalmoves = new ArrayList<String>();
		List<String> hlist = new ArrayList<String>();
		List<String> slist = new ArrayList<String>();
		List<String> dlist = new ArrayList<String>();
		List<String> clist = new ArrayList<String>();
		List<String> tempList = new ArrayList<String>();

		for (int i = 0; i < p.size(); i++) {
			if (p.get(i).contains("h"))
				hlist.add(p.get(i));
			if (p.get(i).contains("s"))
				slist.add(p.get(i));
			if (p.get(i).contains("d"))
				dlist.add(p.get(i));
			if (p.get(i).contains("c"))
				clist.add(p.get(i));
		}

		tempList = individualCheck(hlist, r1.get(0), r2.get(0), r3.get(0));
		legalmoves.addAll(tempList);
		tempList = individualCheck(slist, r1.get(1), r2.get(1), r3.get(1));
		legalmoves.addAll(tempList);
		tempList = individualCheck(dlist, r1.get(2), r2.get(2), r3.get(2));
		legalmoves.addAll(tempList);
		tempList = individualCheck(clist, r1.get(3), r2.get(3), r3.get(3));
		legalmoves.addAll(tempList);
		for (int i = 0; i < legalmoves.size(); i++) {
			System.out.println(legalmoves.get(i));
		}
		if (!legalmoves.isEmpty()) {
			Random rgen = new Random();
			card = legalmoves.get(rgen.nextInt(legalmoves.size()));
		}

		card = bestmove(legalmoves, p);
		return card;
	}

	private String bestmove(List<String> legalmoves, List<String> p) {
		String card = "pass";

		if(legalmoves.size()==0){
			card="pass";
		}
		else if (legalmoves.size() == 1) {
			card = legalmoves.get(0);
		} else {
			System.out.println(legalmoves.size());
			int[] points = new int[legalmoves.size()];
			for (int i = 0; i < legalmoves.size(); i++) {
				points[i] = getpoints(legalmoves.get(i), p);
			}
			card=getbestcard(points,legalmoves);
		}
		
		return card;
	}


	private String getbestcard(int[] points, List<String> p) {
		int max=points[0];
		int index=0;
		for (int i = 0; i < points.length; i++) {
			if(points[i]>max){
				max=points[i];
				index=i;
			}
		}
		return p.get(index);
	}

	private int getpoints(String move, List<String> p) {
		int points = 0;
		int cardface = Integer.parseInt(move.substring(1));
		String face = move.substring(0, 1);
		int temp;

		for (int i = 0; i < p.size(); i++) {
			if (p.get(i).contains(face)) {
				temp = Integer.parseInt(p.get(i).substring(1));
				if (cardface == 7) {
					points=points+getpoint(temp);
				} else if (cardface > 7) {
					if(temp>7){
					points=points+getpoint(temp);
					}
				} else if (cardface < 7) {
					if(temp<7){
					points=points+getpoint(temp);
					}
				}
			}
		}
	
		return points;
	}

	private int getpoint(int temp) {
		if(temp==1){
			return 50;
		}
		else if(temp>10){
			return 20;
		}
		else
			return temp;
		
	}

	public int findlegalMoves(List<String> p, List<String> r1, List<String> r2,
			List<String> r3) {

		List<String> legalmoves = new ArrayList<String>();
		List<String> hlist = new ArrayList<String>();
		List<String> slist = new ArrayList<String>();
		List<String> dlist = new ArrayList<String>();
		List<String> clist = new ArrayList<String>();
		List<String> tempList = new ArrayList<String>();

		for (int i = 0; i < p.size(); i++) {
			if (p.get(i).contains("h"))
				hlist.add(p.get(i));
			if (p.get(i).contains("s"))
				slist.add(p.get(i));
			if (p.get(i).contains("d"))
				dlist.add(p.get(i));
			if (p.get(i).contains("c"))
				clist.add(p.get(i));
		}

		tempList = individualCheck(hlist, r1.get(0), r2.get(0), r3.get(0));
		legalmoves.addAll(tempList);
		tempList = individualCheck(slist, r1.get(1), r2.get(1), r3.get(1));
		legalmoves.addAll(tempList);
		tempList = individualCheck(dlist, r1.get(2), r2.get(2), r3.get(2));
		legalmoves.addAll(tempList);
		tempList = individualCheck(clist, r1.get(3), r2.get(3), r3.get(3));
		legalmoves.addAll(tempList);
		for (int i = 0; i < legalmoves.size(); i++) {
			System.out.println(legalmoves.get(i));
		}
		if (legalmoves.isEmpty()) {
			return 0;
		} else
			return 1;

	}

	public List<String> individualCheck(List<String> l, String r1, String r2,
			String r3) {
		List<String> list = new ArrayList<String>();
		int[] tempr = { 0, 0, 0 };
		for (int i = 0; i < l.size(); i++) {
			int cardnumber = Integer.parseInt(l.get(i).substring(1));
			if (r2.equals("blank")) {
				if (cardnumber == 7) {
					list.add(l.get(i));
				}
			} else {

				// check for upper part
				if (cardnumber > 7 && !r1.equals("blank")) {
					tempr[0] = Integer.parseInt(r1.substring(1));
					if ((cardnumber) == (tempr[0] + 1)) {
						list.add(l.get(i));
					}
				} else if (cardnumber > 7 && r1.equals("blank")) {
					if ((cardnumber) == 8) {
						list.add(l.get(i));
					}
				}
				// check for lower part
				if (cardnumber < 7 && !r3.equals("blank")) {
					tempr[2] = Integer.parseInt(r3.substring(1));
					if ((cardnumber) == (tempr[2] - 1)) {
						list.add(l.get(i));
					}
				} else if (cardnumber < 7 && r3.equals("blank")) {
					if ((cardnumber) == 6) {
						list.add(l.get(i));
					}
				}
			}
		}
		return list;
	}
}
