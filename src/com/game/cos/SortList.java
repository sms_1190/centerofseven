package com.game.cos;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortList {

	public List<String> sort(List<String> p){
		List<String> newList=new ArrayList<String>();
		List<String> hlist = new ArrayList<String>();
		List<String> slist = new ArrayList<String>();
		List<String> dlist = new ArrayList<String>();
		List<String> clist = new ArrayList<String>();
		List<String> tempList = new ArrayList<String>();

		for (int i = 0; i < p.size(); i++) {
			if (p.get(i).contains("h"))
				hlist.add(p.get(i));
			if (p.get(i).contains("s"))
				slist.add(p.get(i));
			if (p.get(i).contains("d"))
				dlist.add(p.get(i));
			if (p.get(i).contains("c"))
				clist.add(p.get(i));
		}

		tempList = individualsort(hlist);
		newList.addAll(tempList);
		tempList = individualsort(dlist);
		newList.addAll(tempList);
		tempList = individualsort(slist);
		newList.addAll(tempList);
		tempList = individualsort(clist);
		newList.addAll(tempList);
		return newList;
	}

	public List<String> individualsort(List<String> list) {
		// TODO Auto-generated method stub
		List<String> newlist = new ArrayList<String>();
		
		if(!list.isEmpty()){
			String face=list.get(0).substring(0,1);
			List<Integer> intlist=new ArrayList<Integer>();
			for (int i = 0; i < list.size(); i++) {
				int number=Integer.parseInt(list.get(i).substring(1));
				intlist.add(number);
			}
	Collections.sort(intlist);
	String temp;
	for (int i = 0; i < list.size(); i++) {
		
		temp=face+intlist.get(i);
		newlist.add(temp);
	}
		}
		return newlist;
	}
}
