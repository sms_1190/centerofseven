package com.game.cos;

import java.util.ArrayList;
import java.util.List;

public class Player {

	private List<String> cards;
	
	public Player(){
		cards=new ArrayList<String>();
	}

	public List<String> getCards() {
		return cards;
	}

	public void setCards(List<String> cards) {
		this.cards = cards;
	}
	
	
}
