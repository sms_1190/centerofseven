package com.game.cos;

public class Score {

	private int score;
	private int id;
	public Score(int score, int id) {
		super();
		this.score = score;
		this.id = id;
	}
	public Score() {
		// TODO Auto-generated constructor stub
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
