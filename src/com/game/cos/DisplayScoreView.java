package com.game.cos;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import com.game.cos.R;

public class DisplayScoreView extends SurfaceView {
	private SurfaceHolder holder;
	public DisplayScoreView(Context context) {
		super(context);
		
		holder = getHolder();
		holder.addCallback(new SurfaceHolder.Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
			
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
			}
		});
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint bg = new Paint();
		Rect BG = new Rect();
		bg.setColor(Color.rgb(187, 236, 255));
		bg.setStyle(Paint.Style.FILL);
		BG = new Rect();
		BG.set(0, 0, canvas.getWidth(), canvas.getHeight());
		canvas.drawRect(BG, bg);
	}

}
